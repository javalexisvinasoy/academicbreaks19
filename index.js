// console.log("Hello")

// Activity:



/*
	1. Create a student grading system using an arrow function. The grade categories are as follows: Failed(74 and below), Beginner (75-80), Developing (81-85), Above Average (86-90), Advanced(91-100) 

	Sample output in the console: Congratulations! Your quarterly average is 85. You have received a Developing" mark.

*/

// Code here:

function gradingSystem (grade) {
	if (grade >= 81) {
		console.log("Congratulations! Your quarterly average is " + grade + ". You have received a Developing mark.")
	} else if (grade >= 75) {
		console.log("Congratulations! Your quarterly average is " + grade + ". You have received a Beginner mark.")
	} else {
		console.log("I'm sorry, your quarterly average is " + grade + ". You have received a Failed mark.")
	}
}

yourGrade = gradingSystem(80);
yourGrade = gradingSystem(72);
yourGrade = gradingSystem(89);

/*
	2. Create an odd-even checker that will check which numbers from 1-300 are odd and which are even,

	Sample output in the console: 
		1 - odd
		2 - even
		3 - odd
		4 - even
		5 - odd
		etc.
*/

// Code here:

for (let count = 1; count <= 300; count++){
if(count % 2 == 0) {
    console.log("The number is even: " + count);
}

else {
    console.log("The number is odd: " + count);
}
}

/*
	3. Create a an object named ""hero"" and input the details using promp(). Here are the details needed: heroName, origin, description, skills(object which will contain 3 uinique skills). Convert hero JS object to JSON data format and log the output in the console.

	Sample output in the console:
		{
		        "heroName": "Aldous",
		        "origin: "Minoan Empire,
		        "description: "A guard of the Minos Labyrinth who kept his pledge even after the kingdom's fall.,
		        "skills": {
		                "skill1": "Soul Steal",
		                |"Skill2": "Explosion",
		                "Skill3": "Chase Fate"
		        }
		}
*/

let hero = JSON.stringify({
	heroName: "Aldous",
	origin: "Minoan Empire",
	description: "A guard of the Minos Labyrinth who kept his pledge even after the kingdom's fall.",
	skills: {
		    "skill1": "Soul Steal",
		    "skill2": "Explosion",
		    "skill3": "Chase Fate"
		    }
})

console.log(hero);